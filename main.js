// Modules to control application life and create native browser window
const {app, BrowserWindow, ipcMain} = require('electron'),
path = require('path'),
config = require('./app/config'),
fs = require('fs');

let app_cache = {
  admin: {
    enabled: false,
    pid: null
  },
  blog: {
    enabled: false,
    pid: null
  },
  mobile: {
    enabled: false,
    pid: null
  },
  rest: {
    enabled: false,
    pid: null
  }
};
process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;
// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow

function createWindow () {
  // Create the browser window.
  mainWindow = new BrowserWindow(config.cnf)

  // and load the index.html of the app.
  mainWindow.loadFile('app/index.html')
  console.log(process.pid)
  // Open the DevTools.
  mainWindow.webContents.openDevTools()

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  })


  ipcMain.on('check_server', function(event, arg){
    let arr = [];
    let iarr = ['admin', 'blog', 'mobile']

    for (let i = 0; i < iarr.length; i++) {
      if(app_cache[iarr[i]].enabled){
        arr.push({
          type: iarr[i],
          pid: app_cache[iarr[i]].pid
        })
      }
    }
    event.reply('check_server', arr)
  })

  ipcMain.on('stop_server', function(event, arg){
    try {
      let dest = '/home/angeal/development/spartan-cms/.tmp/pid/'+  arg.type + '_pid';
      process.kill(arg.pid);
      process.kill(
        JSON.parse(
          fs.readFileSync(dest, 'utf8')
        )
      )
      fs.writeFileSync(dest, 'null');
      if(arg.type === 'admin'){
        dest = '/home/angeal/development/spartan-cms/.tmp/pid/nodemon_pid';
        process.kill(arg.pid);
        process.kill(
          JSON.parse(
            fs.readFileSync(dest, 'utf8')
          )
        )
        fs.writeFileSync(dest, 'null');
      }
    } catch (err) {
      console.log(err);
      return event.reply('toast-msg', {
        type: 'danger',
        msg: 'server error'
      })
    }
    app_cache[arg.type].pid = null;
    app_cache[arg.type].enabled = false;
    event.reply('stop_server', {
      type: arg.type
    })
  })

  ipcMain.on('start_server', function(event, arg){
    const { spawn, exec } = require('child_process');

    let bat;

    if(app_cache[arg].enabled){
      return event.reply('toast-msg', {
        type: 'warning',
        msg: 'server already started'
      })
    }

    if(arg === 'admin'){
      bat = exec('node index.js', {
        cwd: '/home/angeal/development/spartan-cms',
        detached: true
      });

    } else {
      bat = spawn('node', ['./admin/server/'+ arg +'.js'], {
        cwd: '/home/angeal/development/spartan-cms'
      });
    }

    app_cache[arg].pid = bat.pid;
    app_cache[arg].enabled = true;

    bat.stdout.on('data', function(data){
      console.log(data.toString());
    });

    bat.stderr.on('data', function(data){
      console.error(data.toString());
    });

    bat.on('exit', function(code){
      console.log('[process:' + bat.pid + '] terminating...');
    });

    event.reply('start_server', {
      type: arg,
      pid: bat.pid
    })

  })

  ipcMain.on('load-win', function(event, arg){
    let cms_win = new BrowserWindow({
      width: 800,
      height: 600,
      webPreferences:{
        webSecurity: false,
        allowRunningInsecureContent: true
      }
    })

    cms_win.on('certificate-error', (event, webContents, url, error, certificate, callback) => {
      event.preventDefault();
      callback(true);
    });
    cms_win.on('closed', function() {
      cms_win = null
    })
    cms_win.loadURL('https://localhost:'+ arg)
  })

}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') app.quit()
})

app.on('activate', function () {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) createWindow()
})

app.on('certificate-error', function(event, webContents, url, error,
  certificate, callback) {
      event.preventDefault();
      callback(true);
});
