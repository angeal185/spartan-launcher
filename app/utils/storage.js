const g = require('./global');

const ls = {
  get: function(i){
    return g.jp(g.LS.getItem(i))
  },
  set: function(i,e){
    g.LS.setItem(i, g.js(e))
    return;
  },
  del: function(i){
    g.LS.removeItem(i);
  }
}

const ss = {
  get: function(i){
    return g.jp(g.SS.getItem(i))
  },
  set: function(i,e){
    g.SS.setItem(i, g.js(e))
    return;
  },
  del: function(i){
    g.SS.removeItem(i);
  }
}

module.exports = { ls, ss }
