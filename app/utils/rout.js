const config = require('../config'),
fs = require('fs'),
g = require('./global'),
h = require('./h'),
{ls,ss} = require('./storage'),
{ ipcRenderer } = require('electron');

const rout = {
  dashboard: function(main, cb){

    try {
      let card_row = h('div.row');

      for (let i = 0; i < config.server_arr.length; i++) {
        card_row.append(
          utils.return_server_card(config.server_arr[i])
        )
      }

      main.append(card_row);
      ipcRenderer.send('check_server');
      cb(false);
    } catch (err) {
      if(err){return cb(err)}
    }


  },
  settings: function(main, cb){

    try {
      let main_row = h('div.row',
        tpl.install_path()
      );

      main.append(
        h('div.container-fluid',
          h('h3', 'installation'),
          main_row
        )
      )
      cb(false);
    } catch (err) {
      if(err){return cb(err)}
    }
  },
  install: function(main, cb){
    try {
      let main_row = h('div.row', 'install working');

      main.append(main_row)
      cb(false);
    } catch (err) {
      if(err){return cb(err)}
    }
  }
}

module.exports = rout;
