const g = require('./global'),
fs = require('fs'),
h = require('./h'),
{ls,ss} = require('./storage'),
rout = require('./rout'),
{ ipcRenderer } = require('electron'),
{ dialog } = require('electron').remote;



const utils = {
  clone_obj: function(obj){
    return Object.assign({}, obj);
  },
  get_year: function(){
    let d = new Date();
    return d.getFullYear();
  },
  empty: function(i){
    while (i.firstChild) {
      i.removeChild(i.firstChild);
    }
  },
  select_dir: function(cb){
    dialog.showOpenDialog({properties: ['openDirectory']})
    .then(function(res){
      if(res.canceled){
        let err = 'folder select cancelled'
        utils.toast('warning', err);
        return cb(err);
      }
      console.log(res.filePaths[0])
      cb(false, res.filePaths[0])
    })
    .catch(function(err){
      cb(err)
    })
  },
  rout: function(dest, cb){
    let main = document.getElementById('main-content');
    main.innerHTML = '';
    location.hash = dest;
    rout[dest](main, function(err){
      if(err){return cb(err)}
      utils.totop(0);
      cb(false);
    });
  },
  totop: function(i){
    window.scroll({
      top: i,
      left: 0,
      behavior: 'smooth'
    });
  },
  debounce: function(func, wait, immediate) {
  	var timeout;
  	return function() {
  		var context = this, args = arguments;
  		var later = function() {
  			timeout = null;
  			if (!immediate) func.apply(context, args);
  		};
  		var callNow = immediate && !timeout;
  		clearTimeout(timeout);
  		timeout = setTimeout(later, wait);
  		if (callNow) func.apply(context, args);
  	};
  },
  capitalize: function(str) {
   try {
     let x = str[0] || str.charAt(0);
     return x  ? x.toUpperCase() + str.substr(1) : '';
   } catch (err) {
     if(err){return str;}
   }
  },
  return_server_card: function(obj){

    return h('div.col-md-6',
      h('div.card.mb-4',
        h('div.card-body',
          h('h5.card-title', obj.title),
          h('h6.card-subtitle.text-muted', obj.sub),
          h('p','status: ',
            h('span#'+ obj.server +'_status', 'stopped')
          ),
          h('p','pid: ',
            h('span#'+ obj.server +'_pid', 'null')
          ),
          h('div.mt-4',
            h('button.btn.btn-outline-secondary.btn-sm.mr-2.sh-95',{
                type: 'button',
                onclick: function(){
                  let dpid = g.jp(document.getElementById(obj.server +'_pid').innerText);
                  if(dpid === null){
                    return ipcRenderer.send('start_server', obj.server)
                  }
                  utils.toast('warning', 'server already started')
                }
            }, 'Start server'),
            h('button.btn.btn-outline-secondary.btn-sm.sh-95',{
                type: 'button',
                onclick: function(){
                  let dpid = g.jp(document.getElementById(obj.server +'_pid').innerText);
                  if(dpid !== null){
                    return ipcRenderer.send('stop_server', {type: obj.server, pid: dpid})
                  }
                  utils.toast('warning', 'server not started')
                }
            }, 'Stop server'),
            h('button.btn.btn-outline-secondary.btn-sm.float-right.sh-95',{
                type: 'button',
                onclick: function(){
                  ipcRenderer.send('load-win', obj.port)
                }
            }, 'Open app')
          )
        )
      )
    )
  },
  toast: function(i, msg){
    const toast = h('div#toast.alert.alert-'+ i, {
        role: "alert"
    }, msg);
    document.body.append(toast);
    setTimeout(function(){
      toast.classList.add('fadeout');
      setTimeout(function(){
        toast.remove();
      },1000)
    },3000)
    return;
  },
  update_install_path: function(item){
    let obj = utils.clone_obj(config);
    obj.install.path = item;
    fs.writeFile('./app/config/index.json', g.js(obj,0,2), function(err){
      if(err){return utils.toast('danger', 'unable to update install path');}
      utils.toast('success', 'install path updated');
      setTimeout(function(){
        location.reload()
      },3000)
    })
  }
}

module.exports = utils;
