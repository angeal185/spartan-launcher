const config = require('../config'),
h = require('./h'),
utils = require('./'),
{ shell } = require('electron');

const tpl = {
  footer: function(){
    const copyright = config.settings.copyright_msg.replace('{{year}}', utils.get_year());
    return h('div.footer',
      h('div.container-fluid',
        h('div.text-center',
          h('p', copyright)
        )
      )
    )
  },
  install_path: function(){
    return h('div.col-12',
      h('div.form-group',
        h('label', 'installation path'),
        h('div.input-group.input-group-sm',
          h('input.form-control', {
            type: 'text',
            value: config.install.path,
            readOnly: true
          }),
          h('div.input-group-append',
            h('span.input-group-text.cp', {
              onclick: function(){
                let $this = this;
                utils.select_dir(function(err, res){
                  if(err){return g.cl(err)}
                  $this.parentNode.previousSibling.value = res;
                  utils.update_install_path(res);
                })
              }
            }, 'select')
          )
        )
      )
    )
  }
}

module.exports = tpl;
