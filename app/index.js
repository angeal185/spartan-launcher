const config = require('./config'),
g = require('./utils/global'),
tpl = require('./utils/tpl'),
utils = require('./utils'),
h = require('./utils/h'),
{ side_bar, bread_crumb, to_top } = require('./utils/components'),
{ ipcRenderer } = require('electron');

document.body.append(
  new side_bar(config.sb_nav),
  h('nav#navbar.navbar.fixed-top.bg-black.justify-content-center.mb-4',
    h('span',
      h('img.img-fluid', {
        src: 'static/images/lg_50.png'
      })
    )
  ),
  new bread_crumb(),
  h('div#main-content.container-fluid'),
  tpl.footer(),
  h('div#sub-content',
    new to_top(0)
  )
)


ipcRenderer.on('check_server', function(event, arg){
  if(arg.length > 0){
    for (let i = 0; i < arg.length; i++) {
      document.getElementById(arg[i].type+'_pid').innerText = arg[i].pid;
      document.getElementById(arg[i].type+'_status').innerText = 'started';
    }
    utils.toast('success', 'server found')
  }
})

ipcRenderer.on('stop_server', function(event, arg){
  document.getElementById(arg.type+'_pid').innerText = 'null';
  document.getElementById(arg.type+'_status').innerText = 'stopped';
  utils.toast('success', 'server stopped');
})

ipcRenderer.on('start_server', function(event, arg){
  document.getElementById(arg.type+'_pid').innerText = arg.pid;
  document.getElementById(arg.type+'_status').innerText = 'started';
  utils.toast('success', 'server started')
})

ipcRenderer.on('toast-msg', function(event, arg){
  utils.toast(arg.type, arg.msg)
})


utils.rout(config.settings.landing, function(err){
  if(err){return g.ce(err)}
  g.cl('dashboard loaded')
});
